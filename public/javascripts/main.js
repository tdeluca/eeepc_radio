$(document).ready(function(){

    //clicking on a radio-channel
    $('#radio-channels').on('click', '.channel-link', function(){
        var playlist = $(this).attr('data-pls');
        $('#start').attr('data-pls', playlist);
	$.post('/controls/play', {playlist: playlist});
    });

    //start btn
    $('#start').click(function(){
        $('.channel-link[data-pls="' + $(this).attr('data-pls') + '"]').trigger('click');
    });

    //stop btn
    $('#stop').click(function(){
        console.log($(this));
        $.post('/controls/stop');
    });

    //volume buttons
    $('.volume-button').click(function(){
        var updown = $(this).attr('data-updown');
        $.post('/controls/volume/' + updown);
        
    });

    //get local IP address
    $('#get_ip').click(function(){
        $.get('/controls/get_ip', {}, function(d){alert(d)});
        return false;
    });


    //search input
    $('#search').keyup(function(){
        var str = $(this).val();
        $('#radio-channels .radio-channel').show();
        if(str == '') return;
        var str = str.toLowerCase();
        $('#radio-channels .radio-channel').each(function(){
            var station = $(this).find('.channel-name').text().toLowerCase();
            var channel = $(this).find('.channel-station').text().toLowerCase();
            if(station.indexOf(str) == -1 && channel.indexOf(str) == -1) $(this).hide();
        });
    });

    //add-to-fav buttons
    $('#radio-channels').on('click', '.no-fav', function(event){
        $(this).blur();
        event.stopPropagation();
        var obj = $(this);
        $.post('/fav/add', {name: $(this).attr('data-name')}, function(){
            obj.removeClass('no-fav').addClass('fav');
        });
    });

    //remove-station-from-fav button
    $('#radio-channels').on('click', '.remove-fav', function(event){
        $(this).blur();
        event.stopPropagation();
        var obj = $(this);
        $.post('/fav/remove', {name: $(this).attr('data-name')}, function(){
            obj.closest('.radio-channel').remove();
        });
    });

    //station-is-fav button
    $('#radio-channels').on('click', '.fav', function(event){
        $(this).blur();
        event.stopPropagation();
        var obj = $(this);
        $.post('/fav/remove', {name: $(this).attr('data-name')}, function(){
            obj.removeClass('fav').addClass('no-fav');
        });
    });
});
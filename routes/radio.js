var express = require('express');
var http = require('http');
var router = express.Router();
var fs = require('fs');
var spawn = require('child_process').spawn;
var os = require('os');
var vlc = spawn('vlc', ['-I rc']);
//vlc.stdout.on('data', function (data) {
//    console.log('stdout: ' + data);
//});

var channels = JSON.parse(fs.readFileSync(__dirname+'/../public/stations/channels.json', 'utf-8'));

function sort_channels(a, b){
    if(a.name < b.name) return -1;
    if(a.name > b.name) return 1;
    return 0;
}

function get_favs(){
    return JSON.parse(fs.readFileSync(__dirname+'/../public/stations/fav.json', 'utf8'));
}

local_ip(); //call one time on startup
function local_ip(){
    var ip = '';
    var ifaces = os.networkInterfaces();
    //first try ethernet
    if(ifaces.eth0) ip = ifaces.eth0[0].address + ':3000';
    else if(ifaces.wlan0) ip = ifaces.wlan0[0].address + ':3000';
    var postData = 'ip='+ip;
    var options = {
        hostname: 'www.thomasdeluca.nl',
        port: 80,
        path: '/pi/index.php',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': postData.length
        }
    };
    var req = http.request(options);
    req.write(postData);
    req.end();
    return ip;
};

router.get('/', function(req, res){
    var favs = get_favs();
    var favs_channels = [];
    channels.forEach(function(channel){
        if(favs.indexOf(channel.name) > -1){
            favs_channels.push(channel);
        }
    });
    favs_channels.sort(sort_channels);
    res.render('radio', { channels: favs_channels, btn_type: 'remove-fav', icon_type: 'remove'});
});

router.get('/channels', function(req, res) {
    var favs = get_favs();
    var _channels = JSON.parse(JSON.stringify(channels)); //copy channels object
    _channels.forEach(function(channel){
        channel.btn_type = favs.indexOf(channel.name) > -1 ? 'fav' : 'no-fav';
    });
    _channels.sort(sort_channels);
    res.render('radio', { channels: _channels, icon_type: 'star'});
});

router.post('/fav/remove', function(req, res){
    var favs = get_favs();
    var index = favs.indexOf(req.body.name);
    favs.splice(index, 1);
    fs.writeFileSync(__dirname+'/../public/stations/fav.json', JSON.stringify(favs));
    res.end();
});

router.post('/fav/add', function(req, res){
    var favs = get_favs();
    favs.push(req.body.name);
    fs.writeFileSync(__dirname+'/../public/stations/fav.json', JSON.stringify(favs));
    res.end();
});

router.post('/controls/play', function(req, res){
    var playlist = req.body.playlist;
    vlc.stdin.write('clear\n');
    vlc.stdin.write('add '+playlist+'\n');
    res.end();
});

router.post('/controls/stop', function(req, res){
    vlc.stdin.write('clear\n');
    res.end();
});

router.post('/controls/volume/up', function(req, res){
    vlc.stdin.write('volume\n');
    vlc.stdout.once('data', function(data){
        data = data.toString();
        var vol = parseInt(data.split('\n')[0], 10);
        var new_vol = vol + 20;
        if(new_vol > 256) new_vol = 256;
        vlc.stdin.write('volume '+new_vol+'\n');
    });
    res.end();
});

router.post('/controls/volume/down', function(req, res){
    vlc.stdin.write('volume\n');
    vlc.stdout.once('data', function(data){
        data = data.toString();
        var vol = parseInt(data.split('\n')[0], 10);
        var new_vol = vol - 20;
        vlc.stdin.write('volume '+new_vol+'\n');
    });
    res.end();
});

router.get('/controls/get_ip', function(req, res){
    res.end('Connect to:\n\r' + local_ip());
});
module.exports = router;
